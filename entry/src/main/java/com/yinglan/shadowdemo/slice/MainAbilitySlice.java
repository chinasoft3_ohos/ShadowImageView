/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yinglan.shadowdemo.slice;

import com.yinglan.shadowdemo.ResourceTable;
import com.yinglan.shadowimageview.DependentView;
import com.yinglan.shadowimageview.DependentView2;
import com.yinglan.shadowimageview.RoundImageView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Slider;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * FA
 *
 * @author wangguan
 * @since 2021-03-31
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private RoundImageView shadow;
    private RoundImageView shadowd2;
    private Slider seekBar1;
    private int resId = 1;
    private Image roundImage;
    private DependentView layoutView;
    private DependentView2 layoutView1;
    private final int resId1 = 1;
    private final int resId2 = 2;
    private final int resId3 = 3;
    private final int resId4 = 4;
    private final int res1 = 1;
    private final int res2 = 2;
    private final int res3 = 3;
    private final int res4 = 4;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        init();
        shadow.setClickedListener(this);
        seekBar1.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean isB) {
                shadow.setImageRadius(i);
                layoutView.setRadius(i);
                layoutView1.setRadius(0);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
    }

    /**
     * 初始化
     */
    public void init() {
        shadow = (RoundImageView)findComponentById(ResourceTable.Id_shadow);
        shadowd2 = (RoundImageView)findComponentById(ResourceTable.Id_shadow2);
        seekBar1 = (Slider)findComponentById(ResourceTable.Id_seekbar1);
        layoutView = (DependentView)findComponentById(ResourceTable.Id_layoutView);
        layoutView1 = (DependentView2) findComponentById(ResourceTable.Id_layoutView2);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_shadow) {
            int res = ResourceTable.Media_lotus;
            switch (resId) {
                case res1:
                    res = ResourceTable.Media_mountain;
                    resId = resId2;
                    break;
                case res2:
                    res = ResourceTable.Media_sunset;
                    resId = resId3;
                    break;
                case res3:
                    res = ResourceTable.Media_red;
                    resId = resId4;
                    break;
                case res4:
                    res = ResourceTable.Media_lotus;
                    resId = resId1;
                    break;
                default:
                    break;
            }
            shadow.setImageResource(res);
            try {
                layoutView.setDrawColor(res);
            } catch (NotExistException e) {
                e.getMessage();
            } catch (WrongTypeException e) {
                e.getMessage();
            } catch (IOException e) {
                e.getMessage();
            }
        }
    }
}
