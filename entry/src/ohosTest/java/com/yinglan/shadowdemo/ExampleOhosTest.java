/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yinglan.shadowdemo;

import com.yinglan.shadowimageview.RoundImageView;
import com.yinglan.shadowimageview.util.AttrUtil;
import com.yinglan.shadowimageview.util.Utils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private RoundImageView roundImageView;
    private AttrUtil attrUtil;
    private Utils utils;

    @Before
    public void initRoundImageView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        roundImageView  = new RoundImageView(context);
        utils = new Utils();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.yinglan.shadowdemo", actualBundleName);
    }

    @Test
    public void setRound() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        float res =  Utils.getInstance().dp2Px(context,2f);
        System.out.println("res = "+res);
    }

    @Test
    public void setImageResource() {
        roundImageView.setImageResource(1);
        System.out.println("res = ");
    }

    @Test
    public void setImageRadius() {
        roundImageView.setImageRadius(1);
        System.out.println("res = ");
    }

    @Test
    public void getScreenPiex(){
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        utils.getScreenPiex(context);
    }

    @Test
    public void dp2Px(){
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        utils.dp2Px(context,2f);
    }

    @Test
    public void getStringValue(){
        attrUtil.getStringValue(new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        }, "1","1");
    }

    @Test
    public void getResourceId(){
        attrUtil.getResourceId(new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        },"1",1);
    }

    @Test
    public void getMediaId(){
        attrUtil.getMediaId(new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        },"1",1);
    }

    @Test
    public void getFloatValue(){
        attrUtil.getFloatValue(new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        }, "1",1);
    }

    @Test
    public void getDimension(){

        attrUtil.getDimension(new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        }, "1",1);
    }





}