## 1.0.0

ohos 第三个版本
- 正式版本

## 0.0.2-SNAPSHOT

ohos 第二个版本,更新SDK6

## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 因为 Palette调色板未提供通过Swatch提供的方法获取颜色的相关信息原因，Palette.Swatch没有实现从调色板中占主导地位的样本的颜色