/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yinglan.shadowimageview;

import com.yinglan.shadowimageview.util.AttrUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * DependentView
 *
 * @author name
 * @since 2021-04-02
 */
public class DependentView2 extends DependentLayout implements Component.DrawTask,Component.EstimateSizeListener {
    /**
     * 全部阴影
     */
    public static final int ALL = 0x1111;
    /**
     * 阴影左侧
     */
    public static final int LEFT = 0x0001;
    /**
     * 阴影上方
     */
    public static final int TOP = 0x0010;
    /**
     * 阴影右侧
     */
    public static final int RIGHT = 0x0100;
    /**
     * 阴影下方
     */
    public static final int BOTTOM = 0x1000;
    /**
     * Round
     */
    private static final int ROUND = 20;
    private int mShadowSide = BOTTOM;
    private int mShadowDy = 0;
    private RectFloat mRectF = new RectFloat();
    private Paint shadowPaint = new Paint();
    private int shadowRound = 0;

    /** DependentView
     *
     * @param context
     */
    public DependentView2(Context context) {
        super(context);
    }

    /** DependentView
     *
     * @param context
     * @param attrSet
     */
    public DependentView2(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(attrSet);
        setEstimateSizeListener(this);
        addDrawTask(this::onDraw);
    }

    /** DependentView
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DependentView2(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    private void initView(AttrSet attrs) {
        mShadowSide = AttrUtil.getIntegerValue(attrs, "shadowSide", BOTTOM);
        shadowPaint.setColor(Color.RED);
        shadowPaint.setStyle(Paint.Style.FILL_STYLE);
        shadowPaint.setDither(true);
        shadowPaint.setAntiAlias(true);
    }

    /** setRadius
     *
     * @param radius
     */
    public void setRadius(int radius) {
        shadowRound = radius;
        invalidate();
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        int effect = shadowRound + ROUND;
        float rectLeft = 0;
        float rectTop = 0;
        int rectRight = this.getWidth();
        int rectBottom = this.getHeight() + ROUND;
        this.getWidth();
        /**
         * 计算所需的边距值
         */
        if ((mShadowSide & LEFT) == LEFT) {
            rectLeft = effect;
        }
        if ((mShadowSide & TOP) == TOP) {
            rectTop = effect;
        }
        if ((mShadowSide & RIGHT) == RIGHT) {
            rectRight = this.getEstimatedWidth() - effect;
        }
        if ((mShadowSide & BOTTOM) == BOTTOM) {
            rectBottom = this.getEstimatedHeight() - effect;
        }
        if (String.valueOf(mShadowDy) != null) {
            rectBottom = rectBottom - mShadowDy;
        }
        mRectF.left = rectLeft;
        mRectF.top = rectTop;
        mRectF.right = rectRight;
        mRectF.bottom = rectBottom;
        final int paddingBot = 50;
        this.setPaddingBottom(paddingBot);
        invalidate();
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        final float mask = 50f;
        final int strokeWidth = 10;
        postLayout();
        Paint mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setMaskFilter(new MaskFilter(mask, MaskFilter.Blur.OUTER));
        mPaint.setStrokeWidth(strokeWidth);
        mPaint.setColor(Color.BLUE);
        Path mPath = new Path();
        final int y1 = 600;
        final int y2 = 630;
        final int x1 = 994;
        mPath.moveTo(0, y1);
        mPath.lineTo(0, y2);
        mPath.lineTo(x1, y2);
        mPath.lineTo(x1, y1);
        canvas.drawPath(mPath, mPaint); // 直接画阴影效果
    }
}
