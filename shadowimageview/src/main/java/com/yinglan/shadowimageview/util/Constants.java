/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yinglan.shadowimageview.util;

/**
 * 类功能描述(类上)
 *
 * @author name
 * @since 2021-04-02
 */
public final class Constants {
    /**
     * SHADOWCOLOR
     */
    public static final int SHADOWCOLOR = 147483648;
    /**
     *
     * IMAGERESOURCE
     */
    public static final int IMAGERESOURCE = -1;
    /**
     *
     * TWO
     */
    public static final int TWO = 2;
    /**
     *
     * NINETY
     */
    public static final int NINETY = 90;
    /**
     *
     * NEGNINETY
     */
    public static final int NEGNINETY = -90;
    /**
     *
     * NEGZERO
     */
    public static final int NEGZERO = -0;
    /**
     *
     * SCALDENSITY
     */
    public static final float SCALDENSITY = 3.0f;
    /**
     *
     * XCOOL
     */
    public static final int XCOOL = 950;
    /**
     *
     * YCOOL
     */
    public static final int YCOOL = 960;

    private Constants() {
    }
}
