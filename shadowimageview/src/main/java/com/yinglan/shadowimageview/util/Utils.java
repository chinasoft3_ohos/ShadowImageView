/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yinglan.shadowimageview.util;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.Optional;

/**
 * 获取屏幕分辨率工具类
 *
 * @author name
 * @since 2021-03-31
 */
public class Utils {
    private static Utils instance = null;

    /**
     * 获取自定义属性
     *
     * @return utils
     *
     */
    public static synchronized Utils getInstance() {
        if (instance == null) {
            instance = new Utils();
        }
        return instance;
    }

    /**
     * 获取屏幕密度
     *
     * @param context
     * @return utils
     *
     */
    public static DisplayAttributes getScreenPiex(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes;
    }

    /**
     * 获取自定义属性
     *
     * @param context
     * @param dp
     * @return 屏幕密度
     *
     */
    public float dp2Px(Context context, float dp) {
        // 获取屏幕密度
        DisplayAttributes displayAttributes = getScreenPiex(context);
        return dp * displayAttributes.scalDensity;
    }

    /**
     * 获取自定义属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return string
     *
     */
    public String getAttrStringValue(AttrSet attrSet, String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return utils
     *
     */
    public int getAttrIntValue(AttrSet attrSet, String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义属性
     *
     * @param attrSet
     * @param key
     * @param isDefValue
     * @return utils
     *
     */
    public boolean getAttrBoolValue(AttrSet attrSet, String key, boolean isDefValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getBoolValue();
        } else {
            return isDefValue;
        }
    }

    /**
     * 获取自定义属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return utils
     *
     */
    public Element getAttrElementValue(AttrSet attrSet, String key, Element defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getElement();
        } else {
            return defValue;
        }
    }

    /**
     * 获取自定义属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return utils
     *
     */
    public Color getAttrColorValue(AttrSet attrSet, String key, Color defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue();
        } else {
            return defValue;
        }
    }

    /**
     * 资源ID转PixeMap
     *
     * @param context
     * @param mediaId
     * @return utils
     * @throws NotExistException
     * @throws WrongTypeException
     * @throws IOException
     *
     */
    public PixelMap getPixelMapFromMedia(Context context,int mediaId)
            throws NotExistException, WrongTypeException, IOException {
        ImageSource.SourceOptions options = new ImageSource.SourceOptions();
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        ResourceManager manager = context.getResourceManager();
        String path = manager.getMediaPath(mediaId);
        Resource resource = manager.getRawFileEntry(path).openRawFile();
        ImageSource source = ImageSource.create(resource,options);
        return source.createPixelmap(decodingOptions);
    }
}
