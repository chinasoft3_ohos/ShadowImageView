/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yinglan.shadowimageview.util;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

/**
 * 自定义属性工具类
 *
 * @author name
 * @since 2021-03-31
 */
public class AttrUtil {
    static String colon = "::";

    private AttrUtil() {
    }

    /**
     * 获取字符属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return 字符
     */
    public static String getStringValue(AttrSet attrSet, String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取色值属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return 色值
     */
    public static Color getColorValue(AttrSet attrSet, String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getColorValue();
        } else {
            return new Color(Color.getIntColor(defValue));
        }
    }

    /**
     * 获取Boolean属性
     *
     * @param attrSet
     * @param key
     * @param isDefValue
     * @return boolean
     */
    public static boolean getBooleanValue(AttrSet attrSet, String key, boolean isDefValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getBoolValue();
        } else {
            return isDefValue;
        }
    }

    /**
     * 获取元素属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return 元素
     */
    public static Element getElementValue(AttrSet attrSet, String key, Element defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getElement();
        } else {
            return defValue;
        }
    }

    /**
     * 带单位的数字
     *
     * @param attrSet
     * @param key
     * @param defDimensionValue
     * @return 数字
     */
    public static int getDimension(AttrSet attrSet, String key, int defDimensionValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getDimensionValue();
        } else {
            return defDimensionValue;
        }
    }

    /**
     * 获取int数值属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return int
     */
    public static int getIntegerValue(AttrSet attrSet, String key, int defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getIntegerValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取float数值属性
     *
     * @param attrSet
     * @param key
     * @param defValue
     * @return float
     */
    public static float getFloatValue(AttrSet attrSet, String key, float defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getFloatValue();
        } else {
            return defValue;
        }
    }

    /**
     * 获取资源文件id 格式 $layout:布局名称
     *
     * @param attrSet
     * @param resourceKey
     * @param defvalue
     * @return id
     */
    public static int getResourceId(AttrSet attrSet, String resourceKey, int defvalue) {
        int resourceId = defvalue;
        if (attrSet.getAttr(resourceKey).isPresent()) {
            String value = attrSet.getAttr(resourceKey).get().getStringValue();
            if (value.contains(colon)) {
                try {
                    resourceId = Integer.parseInt(value.substring(value.indexOf(":") + 1));
                } catch (NumberFormatException e) {
                    e.getMessage();
                }
            } else {
                throw new IllegalStateException("请确认是否添加正确的资源文件，格式@layout:布局名称");
            }
        }
        return resourceId;
    }

    /**
     * 获取资源文件id 格式 $layout:布局名称
     *
     * @param attrSet
     * @param resourceKey
     * @param defvalue
     * @return id
     */
    public static int getMediaId(AttrSet attrSet, String resourceKey, int defvalue) {
        int resourceId = defvalue;
        if (attrSet.getAttr(resourceKey).isPresent()) {
            String value = attrSet.getAttr(resourceKey).get().getStringValue();
            if (value.contains(colon)) {
                try {
                    resourceId = Integer.parseInt(value.substring(value.indexOf(":") + 1));
                } catch (NumberFormatException e) {
                    e.getMessage();
                }
            } else {
                throw new IllegalStateException("请确认是否添加正确的资源文件，格式$Media:图片名称");
            }
        }
        return resourceId;
    }
}
