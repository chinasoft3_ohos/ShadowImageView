﻿#  ShadowImageView



#### 项目介绍

- 项目名称： ShadowImageView

- 所属系列：openharmony的第三方组件适配移植

- 功能：设置图片、设置图片半径、设置图片阴影颜色、根据图片内容获取阴影颜色。

- 项目移植状态：主功能已完成

- 调用差异：无

- 开发版本：sdk6，DevEco Studio2.2 Beta1

- 基线版本：master分支

  

#### 效果演示

<img src="gif/ShadowImageViewDemo.gif"></img>


#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:shadowimageview:1.0.0')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

**设置图片**
```java
shadow.setImageResource();
```
**设置图片半径**
```java
shadow.setImageRadius()
```

**设置阴影颜色**
```java
setDrawColor()
```


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

```
The work done has been licensed under Apache License 2.0. The license file can be found here. You can find out more about the license at:

http://www.apache.org/licenses/LICENSE-2.0
```